<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## About Laravel

`php:8.0` `laravel:7.29`



## About Product management system project

Laravel project near to medium website with these feature :

- Admin panel to add/edit/delete/list products
- Admin panel to add/edit/delete/list users
- Admin panel to add/edit/delete/list roles
- login/logout to admin panel
- login/logout to admin user interface
- show products in user interface
- show product details in user interface
- add/edit/remove products to cart

## Demo

```
https://downtown-bb.com/
```


## Installation 

open a new directory and write the following commends

```
git clone https://gitlab.com/abd.r.horani/product-management-system.git
```

```
cd product-management-system
```

```
cp .env.example .env
```
create database and name it 'laravel'

```
composer install
```

```
php artisan migrate
```

```
php artisan db:seed --class=RolesSeeder
```

```
php artisan key:generate
```
```
php artisan serve
```
then you can browse the application : on 

[localhost:8000/](localhost:8000/)

you can use one of generated user by the seeds to login to admin panel

note : that all generated users by seeds have the same password : '**password**'

[localhost:8000/admin/](localhost:8000/admin/)
 

if you want to seed some products : 
 
 
add images to the system by file manager and make their names between 1 and 10

then : 

```
php artisan db:seed --class=DatabaseSeeder
```

 
 appriciate your time
   
## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
