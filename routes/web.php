<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/shop');
});

Route::get('/cart', function () {
    return view('interface.cart');
})->name('cart');

Route::get('/shop', 'ProductController@productsList')->name('productsList');
Route::get('/product/{prod_id}', 'ProductController@productsDetails')->name('product');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
