<?php

use Illuminate\Database\Seeder;
use App\Models\Product;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $counter = 10;
        while ($counter > 0) {
            $product = Product::create([
                'title' => 'product-' . rand(1, 9),
                'description' => 'product description',
                'image' => 'uploads/' . rand(1, 9) . '.jpg',
                'stock' => rand(1, 50),
            ]);
            $counter--;
        }
    }
}
