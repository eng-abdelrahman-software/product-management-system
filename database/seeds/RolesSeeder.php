<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Role;

class RolesSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $adminUser = User::create([
            'name' => 'admin',
            'email' => 'admin@gmail.com', // optional
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
        ]);

        $customerUser = User::create([
            'name' => 'user',
            'email' => 'user@gmail.com', // optional
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
        ]);

        $adminRole = Role::create([
            'name' => 'admin',
            'display_name' => 'admin', // optional
            'description' => 'admin', // optional
        ]);

        $customerRole = Role::create([
            'name' => 'customer',
            'display_name' => 'normal user', // optional
            'description' => 'normal user', // optional
        ]);

        $adminUser->attachRole($adminRole);
        $customerUser->attachRole($customerRole);
    }
}
