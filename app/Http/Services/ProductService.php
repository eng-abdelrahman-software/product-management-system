<?php

namespace App\Http\Services;

use App\Http\Repositories\ProductRepository;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Exceptions\CustomException;

class ProductService{

    protected $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function getAll(){
        $products = $this->productRepository->getAll();
        return $products;
    }

    public function getById($id){
        $product = $this->productRepository->getById($id);
        return $product;
    }

}
