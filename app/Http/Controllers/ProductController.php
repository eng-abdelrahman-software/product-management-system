<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Http\Services\ProductService;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{

    protected $productService;
    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function productsList()
    {
        $products = $this->productService->getAll();
        return view('interface.shop', compact('products'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function productsDetails($prod_id)
    {
        $product = $this->productService->getById($prod_id);
        return view('interface.product', compact('product'));
    }

}
