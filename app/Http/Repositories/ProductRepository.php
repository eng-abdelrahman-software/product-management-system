<?php

namespace App\Http\Repositories;

use App\Models\Product;


class ProductRepository{

    protected $product;

    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    public function getAll(){
        $products = $this->product->orderBy('created_at', 'desc')->get();

        return $products;
    }

    public function getById($id){
        $product = $this->product->find($id);
        return $product;
    }
}
