<!DOCTYPE html>
<html lang="en">
<head>

    <!-- Basic Page Needs
    ================================================== -->
    <meta charset="utf-8">
    <title>Products management system | Namaa solutions</title>

    <!-- Mobile Specific Metas
    ================================================== -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Construction Html5 Template">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=5.0">
    <meta name="author" content="Themefisher">
    <meta name="generator" content="Themefisher Constra HTML Template v1.0">

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/images/favicon.png')}}" />

    <!-- Themefisher Icon font -->
    <link rel="stylesheet" href="{{asset('assets/plugins/themefisher-font/style.css')}}">
    <!-- bootstrap.min css -->
    <link rel="stylesheet" href="{{asset('assets/plugins/bootstrap/css/bootstrap.min.css')}}">

    <!-- Animate css -->
    <link rel="stylesheet" href="{{asset('assets/plugins/animate/animate.css')}}">
    <!-- Slick Carousel -->
    <link rel="stylesheet" href="{{asset('assets/plugins/slick/slick.css')}}">
    <link rel="stylesheet" href="{{asset('assets/plugins/slick/slick-theme.css')}}">

    <!-- Main Stylesheet -->
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">

    <style>
        .no-product-found{
            text-align: center;
        }
        .product-remove{
            cursor: pointer;
        }
        .close-btn{
            float: right;
            margin-top: -22px;
            margin-right: 5px;
        }
    </style>
    @yield('style')
</head>

<body id="body">

@yield('body')

<!--
Essential Scripts
=====================================-->
<!-- Main jQuery -->
{{--<script src="plugins/jquery/dist/jquery.min.js"></script>--}}
<script src="{{asset('assets/plugins/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap 3.1 -->
<script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
<!-- Bootstrap Touchpin -->
<script src="{{asset('assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js')}}"></script>
<!-- Instagram Feed Js -->
<script src="{{asset('assets/plugins/instafeed/instafeed.min.js')}}"></script>
<!-- Video Lightbox Plugin -->
<script src="{{asset('assets/plugins/ekko-lightbox/dist/ekko-lightbox.min.js')}}"></script>
<!-- Count Down Js -->
<script src="{{asset('assets/plugins/syo-timer/build/jquery.syotimer.min.js')}}"></script>

<!-- slick Carousel -->
<script src="{{asset('assets/plugins/slick/slick.min.js')}}"></script>
<script src="{{asset('assets/plugins/slick/slick-animation.min.js')}}"></script>

<!-- Google Mapl -->
{{--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCC72vZw-6tGqFyRhhg5CkF2fqfILn2Tsw"></script>--}}
<script type="text/javascript" src="{{asset('assets/plugins/google-map/gmap.js')}}"></script>

<!-- Main Js File -->
<script src="{{asset('assets/js/script.js')}}"></script>

<script>

    // refresh the view of the cart navbar
    function cartNavData ()
    {
        $('.cartItemNavBar').remove();
        var cartData = JSON.parse(localStorage.getItem('cart'));
        console.log('ww', cartData, cartData && cartData.length > 0);
        if (cartData && cartData.length > 0) {
            $('.no-product-found').remove();
            cartData.forEach(function(value, index) {
                $('.cart-summary').append('<div id="cartItemNavBar" class="cartItemNavBar media">\n' +
                    '        <a class="pull-left" href="/product/' + value.id + '">\n' +
                    '            <img class="media-object" src="/'+ value.image + '" alt="image"/>\n' +
                    '        </a>\n' +
                    '        <div class="media-body">\n' +
                    '            <h4 class="media-heading"><a href="/product/' + value.id + '">' + value.title + '</a></h4>\n' +
                    '        </div>\n' +
                    '        <span data-product-id="' + value.id + '" class="close-btn product-remove"><i class="tf-ion-close"></i></span>\n' +
                    '    </div> ');
            });
        }else{
            $('.cart-summary').append(
                '<div class="no-product-found">'+
                '<h5>no product found</h5> ' +
                '</div>');
        }
    }

    // refresh the view of cart page
    function cartviewData ()
    {
        $('.cartTabel').empty();
        var cartData = JSON.parse(localStorage.getItem('cart'));
        console.log('wwwwww', cartData.length);
        if (cartData && cartData.length > 0) {
            cartData.forEach(function(value, index) {
                console.log('value', value);
                $('.cartTabel').append('<tr class="">\n' +
                    '    <td class="">\n' +
                    '        <div class="product-info">\n' +
                    '            <img width="80" src="/'+ value.image + '" alt="" />\n' +
                    '            <a href="/product/' + value.id + '">' + value.title + '</a>\n' +
                    '        </div>\n' +
                    '    </td>\n' +
                    '    <td class="">' + value.description + '</td>\n' +
                    '    <td class=""><input class="product-quantity" data-product-id="' + value.id + '" type="number" min="0" value="'+ value.qty +'"> </td>\n' +
                    '    <td class="">\n' +
                    '        <span class="product-remove" data-product-id="' + value.id + '" >Remove</span>\n' +
                    '    </td>\n' +
                    '</tr>');
            });

        }else{
            $('#no-data-found-table').empty();
            $('#no-data-found-table').append(' <div>\n' +
                '              <h3 style="text-align: center;">  No Items Found </h3>\n' +
                '            </div>');
        }
    }

    // remove product from the cart from the navbar
    $(document).on("click",".product-remove",function(e) {console.log('ss');
        e.preventDefault();

        $('#btn-add-' + $(this).attr("data-product-id")).show();
        $('#btn-remove-' + $(this).attr("data-product-id")).hide();

        $('#addProductToCartSingle').show();
        $('.product-quantity').show();
        $('#removeProductToCartSingle').hide();
        $('#removeProductToCartSingle').empty();

        var cart = JSON.parse(localStorage.getItem('cart')) ? JSON.parse(localStorage.getItem('cart')) : [];
        var  productId = $(this).attr("data-product-id");console.log(productId);
        var index = cart.findIndex(x => x.id ===  productId );
        if (index !== -1){
            cart.splice(index, 1);
            localStorage.setItem("cart", JSON.stringify(cart));
            cartNavData();
            cartviewData();
        }
    });

    // apply the refresh of the information to cart in the navbar and the cart page
    cartNavData();
    cartviewData();

</script>
@yield('script')

</body>
</html>
