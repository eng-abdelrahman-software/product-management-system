@extends('interface')


@section('style')
<style>
    .img{
        height: 350px !important;
    }
</style>
@endsection

@section('content')
{{--    <p>This is my body content.</p>--}}


<section class="page-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="content">
                    <h1 class="page-name">Shop</h1>
                    <ol class="breadcrumb">
                        <li><a href="index.html">Home</a></li>
                        <li class="active">shop</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="products section">
    <div class="container">
        <div class="row">

            @foreach($products as $product)
            <div class="col-md-4">
                <div class="product-item">
                    <div class="product-thumb">
                        <img class="img-responsive img" src="{{asset($product->image)}}" alt="product-img" />
                        <div class="preview-meta">
                            <ul>
                                <li>
                                    <a class="removeProductFromCart"
                                       id="btn-remove-{{$product->id}}"
                                       data-product-id="{{$product->id}}"
                                       data-product-description="{{$product->description}}"
                                       data-product-title="{{$product->title}}"
                                       data-product-image="{{$product->image}}"
                                       data-product-qty='1' style="display: none">
                                        <i class="tf-ion-android-close"></i>
                                    </a>
                                    <a class="addProductToCart"
                                       id="btn-add-{{$product->id}}"
                                       data-product-id="{{$product->id}}"
                                       data-product-description="{{$product->description}}"
                                       data-product-title="{{$product->title}}"
                                       data-product-image="{{$product->image}}"
                                       data-product-qty='1' >
                                        <i class="tf-ion-android-cart"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <a href="{{route('product', $product->id)}}">
                        <div class="product-content">
                            <h4>{{$product->title}}</h4>
                        </div>
                    </a>
                </div>
            </div>
            @endforeach

            <input id="productsId" type="text" value="<?php
            foreach ($products as $product){
                echo $product['id'] . ',';
            }
            ?>" hidden>
        </div>
    </div>
</section>

@endsection


@section('script')
    <script>
        $(document).ready(function () {

            // check if the product is added to cart or not
            var productsId = $('#productsId').val();
            productsId = productsId.split(',');
            productsId.forEach(function(value, index){
                var cart = JSON.parse(localStorage.getItem('cart')) ? JSON.parse(localStorage.getItem('cart')) : [];
                var  productId = value;
                var index = cart.findIndex(x => x.id ===  productId );
                if (index !== -1) {
                    $('#btn-add-' + productId).hide();
                    $('#btn-remove-' + productId).show();
                }
            });

            // add product to cart
            $(".addProductToCart").click(function () {
                $('#btn-add-' + $(this).attr("data-product-id")).hide();
                $('#btn-remove-' + $(this).attr("data-product-id")).show();

                var cart = JSON.parse(localStorage.getItem('cart')) ? JSON.parse(localStorage.getItem('cart')) : [];
                var product = {
                    id: $(this).attr("data-product-id"),
                    title: $(this).attr("data-product-title"),
                    description: $(this).attr("data-product-description"),
                    image: $(this).attr("data-product-image"),
                    qty: $(this).attr("data-product-qty"),
                };
                cart.push(product);
                console.log(cart);
                localStorage.setItem("cart", JSON.stringify(cart));
                // var cartData = JSON.parse(localStorage.getItem('cart'));

                cartNavData();
            });

            // remove product from cart
            $(".removeProductFromCart").click(function () {
                $('#btn-add-' + $(this).attr("data-product-id")).show();
                $('#btn-remove-' + $(this).attr("data-product-id")).hide();

                var cart = JSON.parse(localStorage.getItem('cart')) ? JSON.parse(localStorage.getItem('cart')) : [];
                var  productId = $(this).attr("data-product-id");
                var index = cart.findIndex(x => x.id ===  productId );
                if (index !== -1){
                    cart.splice(index, 1);
                    localStorage.setItem("cart", JSON.stringify(cart));
                    cartNavData();
                    cartviewData();
                }
            });
        });
    </script>
@endsection
