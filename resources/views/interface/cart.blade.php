@extends('interface')


@section('content')
<section class="page-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="content">
                    <h1 class="page-name">Cart</h1>
                    <ol class="breadcrumb">
                        <li><a href="{{route('productsList')}}">Home</a></li>
                        <li class="active">cart</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</section>



<div class="page-wrapper">
    <div class="cart shopping">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="block">
                        <div class="product-list">
                            <form method="post">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th class="">Item Name</th>
                                        <th class="">Item description</th>
                                        <th class="">Item Quantity</th>
                                        <th class="">Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody class="cartTabel">
                                    </tbody>
                                </table>
                                <div id="no-data-found-table"></div>
                                <a id="removeAll" href="" class="btn btn-main pull-right">Remove All</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection



@section('script')
    <script>
        $(document).ready(function () {

            // change event to save the change of qty
            $(document).on("change",".product-quantity",function(e) {
                e.preventDefault();
                var cart = JSON.parse(localStorage.getItem('cart')) ? JSON.parse(localStorage.getItem('cart')) : [];
                var  productId = $(this).attr("data-product-id");
                var index = cart.findIndex(x => x.id ===  productId );
                if (index !== -1){
                    cart[index].qty=$(this).val();
                    localStorage.setItem("cart", JSON.stringify(cart));
                }
            });

            // remove product from cart event
            $(document).on("click",".product-remove",function(e) {
                e.preventDefault();
                var cart = JSON.parse(localStorage.getItem('cart')) ? JSON.parse(localStorage.getItem('cart')) : [];
                var  productId = $(this).attr("data-product-id");
                var index = cart.findIndex(x => x.id ===  productId );
                if (index !== -1){
                    cart.splice(index, 1);
                    localStorage.setItem("cart", JSON.stringify(cart));
                    cartNavData();
                    cartviewData();
                }
            });

            // remove all product from cart event
            $('#removeAll').click(function(e){
                e.preventDefault();
                localStorage.setItem("cart", JSON.stringify([]));
                cartNavData();
                cartviewData();
            });

        });
    </script>
@endsection


