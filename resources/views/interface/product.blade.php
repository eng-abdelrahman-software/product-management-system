@extends('interface')


@section('content')
    <section class="single-product">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <ol class="breadcrumb">
                        <li><a href="{{route('productsList')}}">Home</a></li>
                        <li><a href="{{route('productsList')}}">Shop</a></li>
                        <li class="active">Single Product</li>
                    </ol>
                </div>
            </div>
            <div class="row mt-20">
                <div class="col-md-5">
                    <div>
                        <img src="{{asset($product->image)}}" width="100%">
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="single-product-details">
                        <h2>{{$product->title}}</h2>
                        <p class="product-description mt-20">
                            {{$product->description}}
                        </p>
                        <div class="product-quantity">
                            <span>Quantity:</span>
                            <div class="product-quantity-slider">
                                <input id="product-quantity" type="text" value="0" name="product-quantity">
                            </div>
                        </div>
                        <button id="addProductToCartSingle"
                           data-product-id="{{$product->id}}"
                           data-product-description="{{$product->description}}"
                           data-product-title="{{$product->title}}"
                           data-product-image="{{$product->image}}"
                           data-product-qty='1'
                           class="btn btn-main mt-20">Add To Cart</button>
                        <div id="removeProductToCartSingle">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection


@section('script')
<script>
    $(document).ready(function () {

        // check if the product is added to cart or not
        var cart = JSON.parse(localStorage.getItem('cart')) ? JSON.parse(localStorage.getItem('cart')) : [];
        var  productId = $('#addProductToCartSingle').attr("data-product-id");
        var index = cart.findIndex(x => x.id ===  productId );
        if (index !== -1) {
            $('#addProductToCartSingle').hide();
            $('.product-quantity').hide();
            $('#removeProductToCartSingle').show();
            $('#removeProductToCartSingle').empty();
            $('#removeProductToCartSingle').append('<button class="product-remove btn btn-main mt-20" data-product-id="' + $(this).attr("data-product-id") + '">Remove From Cart</button>');
        }

        // add product to cart event
        $("#addProductToCartSingle").click(function () {
            var cart = JSON.parse(localStorage.getItem('cart')) ? JSON.parse(localStorage.getItem('cart')) : [];
            var product = {
                id: $(this).attr("data-product-id"),
                title: $(this).attr("data-product-title"),
                description: $(this).attr("data-product-description"),
                image: $(this).attr("data-product-image"),
                qty: $('#product-quantity').val(),
            };
            cart.push(product);
            localStorage.setItem("cart", JSON.stringify(cart));

            $(this).hide();
            $('.product-quantity').hide();
            $('#removeProductToCartSingle').show();
            $('#removeProductToCartSingle').empty();
            $('#removeProductToCartSingle').append('<button class="product-remove btn btn-main mt-20" data-product-id="' + $(this).attr("data-product-id") + '">Remove From Cart</button>');

            cartNavData();
            cartviewData();
        });

        // remove product from cart event
        $(document).on("click",".product-remove",function(e) {
            e.preventDefault();

            $('#addProductToCartSingle').show();
            $('.product-quantity').show();
            $('#removeProductToCartSingle').hide();
            $('#removeProductToCartSingle').empty();

            var cart = JSON.parse(localStorage.getItem('cart')) ? JSON.parse(localStorage.getItem('cart')) : [];
            var  productId = $(this).attr("data-product-id");
            var index = cart.findIndex(x => x.id ===  productId );
            if (index !== -1){
                cart.splice(index, 1);
                localStorage.setItem("cart", JSON.stringify(cart));
                cartNavData();
                cartviewData();
            }
        });
    });
</script>
@endsection
